# Setup kubectl

It is assumed that you are provided with a kubernetes cluster by the instructor. Before you are able to do anything on the cluster, you need to be able to *talk* to this cluster from/using your computer. [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) - short for Kubernetes Controller - is *the* command line tool to talk to a Kubernetes cluster.

Most of you should already have the tool installed in your machine. If this is not the case please refer to the related section in the page _TLDR; AWS Single Sign On with Azure AD_ in the internal wiki.

Briefly, for Mac Users, using HomeBrew:

```
brew install kubernetes-cli
```

Kubectl is a *go* binary which allows you to execute commands on your cluster. Your cluster could be a single node VM, such as [minikube](https://github.com/kubernetes/minikube), or a set of VMs on your local computer or somewhere on a host in your data center, a bare-metal cluster, or a cluster provided by any of the cloud providers - as a service - such as GCP, Azure or AWS.

For the remainder of this workshop, we assume you have a Kubernetes cluster on AWS EKS. For instructions on connecting to various types of Kubernetes cluster, check [this article](https://kubernetes.io/docs/tasks/tools/install-kubectl/#configure-kubectl)


## Authenticate to your EKS cluster

**Prerequisites**

* Have succesfully install and configure our AWS SSO with Azure (again refer to _TLDR; AWS Single Sign On with Azure AD_ in the internal wiki)
* Have at least version 1.16.73 of the AWS CLI installed (check with `aws --version`, update with method described in the wiki page above)

**Steps**

1. install aws-iam-authenticator for Amazon EKS:
    1. Download the Amazon EKS-vended aws-iam-authenticator binary from Amazon S3: Example for macOS client: `curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.7/2019-03-27/bin/darwin/amd64/aws-iam-authenticator`
    2. Apply execute permissions to the binary. `chmod +x ./aws-iam-authenticator`
    3. Copy the binary to a folder in your $PATH. We recommend creating a $HOME/bin/aws-iam-authenticator and ensuring that $HOME/bin comes first in your $PATH: `mkdir $HOME/bin && cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$HOME/bin:$PATH`
    4. Add $HOME/bin to your PATH environment variable. For Bash shells for macOS: `echo 'export PATH=$HOME/bin:$PATH' >> ~/.bash_profile`
    5. Test that aws-iam-authenticator binary works: `aws-iam-authenticator help`
2. authenticate `aws-azure-login` (...)
    * (...)
    * choose access `AzureAD-Sandbox-AdminAccess`
3. test access to EKS running `aws eks list-clusters`. You should see the cluster `experiment`.
4. Update your kubeconfig file for your cluster:
    * If it is the first time you want to connect to this cluster from your machine, you will need register this cluster in your local confog file: run `aws eks --region ap-southeast-2 update-kubeconfig --name experiment`
    * if not (You already run those steps a few days/weeks ago). You can just switch back your context to this cluster:
        1. Get the list of your registered cluster: `kubectx`
        2. Find your cluster in the list and switch to it: `kubectx arn:aws:eks:ap-southeast-2:636287438803:cluster/experiment` 
5. Test your configuration: `kubectl get svc -n default`. Output:

```
NAME           TYPE           CLUSTER-IP      EXTERNAL-IP    PORT(S)          AGE
kubernetes     ClusterIP      10.100.0.1      <none>         443/TCP          6h23m
```

*Congratulations, you are in!*


## Verify configuration

You can verify this by looking at the config file: `kubectl config view`. You should see something like this:

```
$ kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: REDACTED
    server: https://B06C3250A749402D4ADF95DE52423328.sk1.ap-southeast-2.eks.amazonaws.com
  name: arn:aws:eks:ap-southeast-2:636287438803:cluster/experiment
- cluster:
    certificate-authority-data: REDACTED
    server: https://api.cluster01.production.autotraderau.io
  name: cluster01.production.autotraderau.io
- cluster:
    certificate-authority-data: REDACTED
    server: https://api.cluster01.staging.autotraderau.io
  name: cluster01.staging.autotraderau.io
contexts:
- context:
    cluster: arn:aws:eks:ap-southeast-2:636287438803:cluster/experiment
    user: arn:aws:eks:ap-southeast-2:636287438803:cluster/experiment
  name: arn:aws:eks:ap-southeast-2:636287438803:cluster/experiment
- context:
    cluster: cluster01.production.autotraderau.io
    namespace: default
    user: cluster01.production.autotraderau.io
  name: cluster01.production.autotraderau.io
- context:
    cluster: cluster01.staging.autotraderau.io
    namespace: carsguide-rasmus
    user: cluster01.staging.autotraderau.io
  name: cluster01.staging.autotraderau.io
current-context: arn:aws:eks:ap-southeast-2:636287438803:cluster/experiment
kind: Config
preferences: {}
users:
- name: arn:aws:eks:ap-southeast-2:636287438803:cluster/experiment
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
      - token
      - -i
      - experiment
      command: aws-iam-authenticator
      env: null
- name: cluster01.production.autotraderau.io
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
      - token
      - -i
      - cluster01.production.autotraderau.io
      command: heptio-authenticator-aws
      env: null
- name: cluster01.staging.autotraderau.io
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
      - token
      - -i
      - cluster01.staging.autotraderau.io
      command: heptio-authenticator-aws
      env: null
```

Furthermore you should now have access to the AWS EKS cluster! Verify by looking at the nodes for the cluster: `kubectl get nodes`

You should be able to see something similar to what is shown below:

```
$ kubectl get nodes
NAME                                                 STATUS    ROLES     AGE       VERSION
ip-192-168-173-206.ap-southeast-2.compute.internal   Ready     <none>    72m       v1.12.7
ip-192-168-252-250.ap-southeast-2.compute.internal   Ready     <none>    72m       v1.12.7
ip-192-168-79-8.ap-southeast-2.compute.internal      Ready     <none>    72m       v1.12.7
```

If you add the `-o wide` parameters to the above command, you will also see the public IP addresses of the nodes:

```
$ kubectl get nodes -o wide
NAME                                                 STATUS    ROLES     AGE       VERSION
NAME                                                 STATUS    ROLES     AGE       VERSION   INTERNAL-IP       EXTERNAL-IP      OS-IMAGE         KERNEL-VERSION                CONTAINER-RUNTIME
ip-192-168-173-206.ap-southeast-2.compute.internal   Ready     <none>    76m       v1.12.7   192.168.173.206   54.252.141.157   Amazon Linux 2   4.14.106-97.85.amzn2.x86_64   docker://18.6.1
ip-192-168-252-250.ap-southeast-2.compute.internal   Ready     <none>    76m       v1.12.7   192.168.252.250   3.104.35.95      Amazon Linux 2   4.14.106-97.85.amzn2.x86_64   docker://18.6.1
ip-192-168-79-8.ap-southeast-2.compute.internal      Ready     <none>    76m       v1.12.7   192.168.79.8      3.104.55.49      Amazon Linux 2   4.14.106-97.85.amzn2.x86_64   docker://18.6.1
```

