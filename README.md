# k8s-exercises

Kubernetes (K8S) for developers tutorial

You can find a summary of many of the commands used in the exercises in the [cheatsheet.md](cheatsheet.md).

## Setup

* [00-setup-kubectl](00-setup-kubectl.md) -
    Skip if you've already installed `kubectl` and have access to a cluster.
* [00-setup-namespace](00-setup-namespace.md) -
    Skip if you've already created a personal namespace and set it as your default.

## Exercises

Head over to [the first exercise](01-pods-deployments.md) to begin.


### Misc

These exercises build upon and have been inspired by the following resources:

* https://github.com/praqma-training/kubernetes-katas
* https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html


