# Persistent storage

## A simple volume on the host
Docker also has a concept of [volumes](https://docs.docker.com/storage/volumes/), if you are familiar you know that command:

```bash
docker container run --name some-nginx -v /some/content:/usr/share/nginx/html:ro -d nginx
```
_but... don't run the command for this exercice, it is unlikey to work on your current machine! (I just wrote it here as a reminder of docker)_


This will create and start a nginx container whose webroot directory `/usr/share/nginx/html` is map to the host directory `/some/content`.
Kubernetes will use similar concepts but because it contains more options the syntax is slightly more complex.

At its core, a volume is just a directory, possibly with some data in it, which is accessible to the Containers in a Pod. How that directory comes to be, the medium that backs it, and the contents of it are determined by the particular volume type used.

To use a volume, a Pod specifies what volumes to provide for the Pod (the `.spec.volumes` field) and where to mount those into Containers (the `.spec.containers.volumeMounts` field).

Take a look at this definition:

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nginx-hostpath
  labels:
    app: nginx-hostpath            # arbitrary label on deployment
spec:
  replicas: 1
  selector:
    matchLabels: # labels the replica selector should match
      app: nginx
      exercise: volume-hostpath
  template:
    metadata:
      labels: # label for replica selector to match
        app: nginx
        exercise: volume-hostpath
    spec:
      containers:
        - image: nginx:1.9.1
          name: test-container
          volumeMounts:
            - mountPath: "/usr/share/nginx/html"
              name: test-volume
      volumes:
        - name: test-volume
          hostPath: # Type of volume
            # directory location on host
            path: /home/ec2-user/data
            # this field is optional
            type: Directory

```
To summarize it will create a nginx container and mount the directory `home/ec2-user/data` from the host to 
`/usr/share/nginx/html` on the container. In a kubernetes cluster the host is the node where your pod lives.

> For the purpose of this exercise I have initially setup the directory `home/ec2-user/data` on every nodes of our 
_experiment_ cluster. And I created a file `hostname.txt` containing the hostname of the node. It will give us a file
to request from our webserver and at the same time hints on where the pod is hosted.

**Your turn:**

> NB: this exercise assumes you have the multitool deployments from exercise 1 running. If not, you can start them with
> 
>     $ kubectl create deployment multitool --image=wbitt/network-multitool
>     deployment.apps/multitool created
> 
> Also please git clone the repo to have all the support-files `git clone git@bitbucket.org:gabcars/k8s-exercises.git` and run the following commands from inside the repository local folder.


To create the `nginx-hostpath` deployment above, use the provided `support-files/hostpath-nginx.yaml` file. Have a look at this file first, it contains a `Service` object too, to expose nginx pod as a ClusterIP, so we'll be able to access it from the multitool pod.
Run it and check the created objects:

```shell
$ kubectl create -f support-files/hostpath-nginx.yaml
deployment.extensions/nginx-hostpath created
service/nginx-hostpath created
$ kubectl get deploy,pod,svc
NAME                                   DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deployment.extensions/multitool        1         1         1            1           47h
deployment.extensions/nginx-hostpath   1         1         1            1           31s

NAME                                  READY     STATUS    RESTARTS   AGE
pod/multitool-cd8497c66-zm9c6         1/1       Running   0          47h
pod/nginx-hostpath-56c8d6b898-ctb8l   1/1       Running   0          31s

NAME                     TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
service/nginx-hostpath   ClusterIP   10.100.109.176   <none>        80/TCP    31s
```

Open a terminal in the multitool pod and request the file `hostname.txt` from your nginx server. Repeat the request multiple times and exit the terminal session.

```bash
$ kubectl exec -it multitool-cd8497c66-zm9c6 bash
bash-4.4# curl 10.100.109.176/hostname.txt
ip-192-168-79-8.ap-southeast-2.compute.internal
bash-4.4# curl 10.100.109.176/hostname.txt
ip-192-168-79-8.ap-southeast-2.compute.internal
bash-4.4# curl 10.100.109.176/hostname.txt
ip-192-168-79-8.ap-southeast-2.compute.internal
bash-4.4# exit
exit
$
```
> You'll have to adapt the pod name and ClusterIP address to match your environment following the output of the command above.

Let's compare this output with the actual hostname of the clusters nodes:

```bash
$ kubectl get nodes
NAME                                                 STATUS    ROLES     AGE       VERSION
ip-192-168-173-206.ap-southeast-2.compute.internal   Ready     <none>    21d       v1.12.7
ip-192-168-252-250.ap-southeast-2.compute.internal   Ready     <none>    21d       v1.12.7
ip-192-168-79-8.ap-southeast-2.compute.internal      Ready     <none>    21d       v1.12.7
```

You should find that one of your node name will match the server response.

Now let's have more fun with this deployment: we will play with the ReplicaSet concept introduced briefly earlier.

Looking back at the content of `support-files/hostpath-nginx.yaml` definition file we specified `.spec.replicas=1`, meaning we want only one instance of this nginx pod.

In the output of the command `kubectl get deploy,pod,svc` above, the column _READY_ of the pods descriptions is telling us that we have currently 1 replicas of the pods for a total of 1 requested.

Update the definition file, and set the replicas value to 4, deploy then the changes in the cluster:

```bash
$ vim support-files/hostpath-nginx.yaml # or use any editor of your choice.
[change replicas value from 1 to 4]
$ kubectl replace -f support-files/hostpath-nginx.yaml
deployment.extensions/nginx-hostpath replaced
The Service "nginx-hostpath" is invalid: spec.clusterIP: Invalid value: "": field is immutable
```

> Don't worry about the error message on the Service object. We have been a bit brutal here, the Service modification failed but since we didn't want to change it at the first place, we don't care for now.

Open a terminal in the multitool pod and request the file `hostname.txt` again from your nginx server. Repeat the request multiple times, until you see different server responses and exit the terminal session.

```bash
$ kubectl exec -it multitool-cd8497c66-zm9c6 bash
bash-4.4# curl 10.100.109.176/hostname.txt
ip-192-168-79-8.ap-southeast-2.compute.internal
bash-4.4# curl 10.100.109.176/hostname.txt
iip-192-168-252-250.ap-southeast-2.compute.internal
bash-4.4# curl 10.100.109.176/hostname.txt
ip-192-168-79-8.ap-southeast-2.compute.internal
bash-4.4# exit
exit
$
```

> This output shows us 2 important things:
> 
> 1. ReplicaSet-wise: the same http requests has been served by different pods, living on different nodes!
> 2. Volumes-wise: it shows that the files is hosted by the node-host, not the container. And this may actually not be the effect intended, because we have very little control on where the pod will be hosted and how to prepare the volumes on multiple nodes.
> This is where `PersistentVolume` and `PersistentVolumeClaim` enter! (see next section) In fact a volume type `hostPath` is not what we want in most situations but it provides us with a simple concept of Kubernetes volume to begin with. There is a similar volume type as `hostPath` but more commonly used: `emptyDir` It is store on the host-node too, will be empty when the pod is created and can be used to shared data between multiple container inside a pod (...)
> 

Delete the deployment and service:

```bash
$ kubectl delete -f support-files/hostpath-nginx.yaml
deployment.extensions "nginx-hostpath" deleted
service "nginx-hostpath" delete
```
For more information see [ReplicaSets on kubernetes.io](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/)

## A persistent volume

List the available storage classes:

```shell
kubectl get storageclass
```

There is only one in our EKS cluster: `gp2` that'll be the default one, so we won't bother mentioning it in our PVC definition below

Create a claim for a dynamically provisioned volume (PVC) for nginx. 

```shell
$ kubectl create -f support-files/pvc-nginx.yaml
```

Check that the PVC exists and is bound:

```shell
$ kubectl get pvc
```

Example:

```shell
$ kubectl get pvc
NAME        STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-nginx   Bound     pvc-736fc3f8-6bfe-11e9-8b47-0281d7e33e16   5Gi        RWO            gp2            40h
```

There should be a corresponding auto-created persistent volume (PV) against this PVC:

```shell
$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS    CLAIM           STORAGECLASS   REASON    AGE
pvc-736fc3f8-6bfe-11e9-8b47-0281d7e33e16   5Gi        RWO            Delete           Bound     gab/pvc-nginx   gp2                      40h
```

We are going to use the file `support-files/nginx-persistent-storage.yaml` file to use storage/volume directives:

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      volumes:
      - name: nginx-htmldir-volume
        persistentVolumeClaim:
          claimName: pvc-nginx
      containers:
      - name: nginx
        image: nginx:1.9.1
        ports:
        - containerPort: 443
        - containerPort: 80
        volumeMounts:
        - mountPath: "/usr/share/nginx/html"
          name: nginx-htmldir-volume
```

Deploy nginx again. 

```shell
$ kubectl create -f support-files/nginx-persistent-storage.yaml
```

After it starts, you may want to examine it by using `kubectl describe pod nginx` and look out for volume declarations.

Now, you access the Nginx instance using curl. You should get a "403 Forbidden", because now the volume you mounted is empty.

> Hint. You learned about exposing deployments on the network in the [service
> discovery](02-service-discovery-and-loadbalancing.md) exercise.

Try to reach the nginx pod by executing a bash shell inside a multitool pod.

> Hint. You learned about the multitool and running command inside a container pod in the [service
> discovery](02-service-discovery-and-loadbalancing.md) exercise.

```shell
$ kubectl exec -it multitool-<ID> bash
bash-4.4# curl 10.0.96.7
<html>
<head><title>403 Forbidden</title></head>
<body bgcolor="white">
<center><h1>403 Forbidden</h1></center>
<hr><center>nginx/1.9.1</center>
</body>
</html>
bash-4.4# 
```

Exit the multitool pod again.

Create a file in the htmldir inside the nginx pod and add some text in it:

```shell
$ kubectl exec -it nginx-deployment-6665c87fd8-cc8k9 -- bash

root@nginx-deployment-6665c87fd8-cc8k9:/# echo "<h1>Welcome to Nginx</h1>This is Nginx with html directory mounted as a volume from EBS Storage."  > /usr/share/nginx/html/index.html
root@nginx-deployment-6665c87fd8-cc8k9:/#
```

Exit the nginx pod again. From the multitool container, run curl again, you should see the web page:

```shell
$ kubectl exec -it multitool-69d6b7fc59-gbghn bash

bash-4.4#
 curl 10.0.96.7
<h1>Welcome to Nginx</h1>This is Nginx with html directory mounted as a volume from EBS Storage.
bash-4.4#
```

Kill the pod:

```shell
$ kubectl delete pod nginx-deployment-6665c87fd8-cc8k9
pod "nginx-deployment-6665c87fd8-cc8k9" deleted
```

Check if it is up (notice a new pod):

```shell
$ kubectl get pods -o wide
NAME                                READY     STATUS    RESTARTS   AGE       IP          NODE
multitool-69d6b7fc59-gbghn          1/1       Running   0          10m       10.0.97.8   gke-dcn-cluster-2-default-pool-4955357e-txm7
nginx-deployment-6665c87fd8-nh7bs   1/1       Running   0          1m        10.0.96.8   gke-dcn-cluster-2-default-pool-4955357e-8rnp
```

Again, from multitool, curl the new nginx pod. You should see the page you created in previous step:

```shell
$ kubectl exec -it multitool-69d6b7fc59-gbghn bash
bash-4.4# curl 10.0.96.8
<h1>Welcome to Nginx</h1>This is Nginx with html directory mounted as a volume from EBS Storage.
bash-4.4# 
```

## Clean up

```shell
$ kubectl delete deployment multitool
$ kubectl delete deployment nginx-deployment
$ kubectl delete service nginx-deployment
$ kubectl delete pvc pvc-nginx
```


## Further reading

[Volumes on kubernetes.io](https://kubernetes.io/docs/concepts/storage/volumes/)




