# Kubernetes Hands-on Operation

We are going to discuss about Kubernetes operation in these areas:

* Preparation
* Deployment
* Troubleshooting

Before we begin, let's confirm we have the following tools:

* helm (deployment tool)
* kubectl (control tool for Kubernetes cluster)
* kubens (helper tool for setting namespace)
* kubectx (helper tool for setting context)
* aws cli (tool to get STS token)
* docker (helper tool for setting context with GUI)
* aws IAM authenticator (tool for authenticating to Kubernetes cluster via AWS IAM)

```shell
helm
kubectl
kubens
kubectx
aws help
aws-iam-authenticator
```

# Recap of Kubernetes Architecture

![alt text](https://blog.tomkerkhove.be/content/images/2018/10/kubernetes-cluster-1.png "Kubernetes Cluster Architecture")

Get used to these terms:

* pod: collection of containers (smallest unit in Kubernetes)
* deployment: package of pod and replica set (close to Autoscaling Group in AWS)
* replica set: measure of how many pod to run
* service: just like a proxy (software load balancer), but expose via Kubernetes IP addresses
* ingress: another proxy, but expose to the world (Internet)

# To Make the Cluster Complete

To make the cluster complete, we provide more "system" applications in the form of containers to help us architect and publish our application in AWS platform:

* Cluster Autoscaler (run as deployment, single pod): help to scale AWS Autoscale based on base cluster metrics: CPU and memory
* Kube2IAM (run as daemonset): help application to get IAM permission by letting the underlying node to assume its role
* External DNS (run as deployment, single pod): help to create AWS Route53 DNS record
* Zalando AWS Ingress Controller (run as deployment and daemonset): help to bring Internet traffics into the cluster using just a single AWS ALB

# Preparation
Pre-configure aws cli (ignore *everything* by pressing ENTER except region = ap-southeast-2 ):

```shell
aws configure
```

In our environment, we implement SSO on our Kubernetes API access. To get the AWS STS (Sandbox) token:

```shell
aws-azure-login
```

Check if you already have the configuration of the Kubernetes cluster that we are working on (*eks02_sandbox_autotraderau_io*):

```shell
kubectx
```

Check your ```~/.kube/config``` file for the imported context.

Get the Kubernetes cluster configuration:

```shell
aws eks --region ap-southeast-2 update-kubeconfig --name eks02_sandbox_autotraderau_io
```

To delete or switch context (cluster):

```shell
kubectx -d eks02_sandbox_autotraderau_io

kubectx eks02_sandbox_autotraderau_io
```

**NOTE**: Get the Kubernetes cluster configuration again if you delete the context.

Prepare your namespace

```shell
kubectl create namespace [lastname]
```

Check your namespace

```shell
kubens
```

Switch to your namespace

```shell
kubens [lastname]
```

Let's deploy!

# Deployment (without Helm)

Using Kubernetes or containerized system is not very different from having a physical machine or a virtual machine.
You are still faced with the same issues such as resource allocation (CPU vs memory) and networking.
What's new, in this case, is the way you make it happen.

There are types of deployment that you can do:

#### Deployment using YAML files
You can deploy a single file or a set of files (such as you have deployment.yaml, service.yaml, ingress.yaml in 1 directory).

```shell
kubectl apply -f https://bitbucket.org/gabcars/k8s-exercises/raw/172ccdd88604cc1a19208bb02a920289203c2eeb/support-files/nginx-simple-deployment.yaml

kubectl apply -f resources/
```

#### Deployment using kubectl command
You can also deploy using ```kubectl``` (see 01-pods-deployments.md)

```shell
kubectl create deploy nginx --image=nginx:1.7.9
```

#### Publish and scale
Once you have a deployment running, you need to expose and scale it. We'll use ClusterIP to expose.

**NOTE**: Because we are using EKS and there's a (ahem!) bug with IP freeze, please scale sensibly. If not some pods may freeze.

```shell
kubectl expose deploy nginx --port 80 --type ClusterIP

kubectl get service

kubectl scale deploy nginx --replicas=2

kubectl get replicasets
```

#### Quick access to your deployment
Usually your cluster runs on a different network and the network itself is not accessible. To get quick access we'll forward a local port (8088) to the service port (80).

```shell
kubectl port-forward service/nginx 8088:80
```

Browse http://localhost:8088

#### Expose your service to the Internet
We will use the help of **Zalando AWS Ingress Controller** to bring public traffic into the pods. Zalando AWS Ingress Controller will scan any Ingress that we have. Traffic will be matched with the host of the ingress. Since we have SSL provided by AWS ACM through ALB, we assign it via annotations in Ingress.

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: nginx
  namespace: [lastname]
  annotations:
    zalando.org/aws-load-balancer-ssl-cert: "arn:aws:acm:ap-southeast-2:636287438803:certificate/42aa9939-a517-4553-ade0-eaa7a253e477"
spec:
  rules:
  - host: [lastname].sandbox.autotraderau.io
    http:
      paths:
      - backend:
          serviceName: nginx
          servicePort: 80
```

#### Something is missing: DNS!
To browse it, you need to use the ALB FQDN and hijack its host header to be *[lastname].sandbox.autotraderau.io*. What a mess!
Let's use **External DNS** to get a DNS record. Add the ingress above with the following annotations:

```yaml
metadata:
  annotations:
    external-dns.alpha.kubernetes.io/hostname: "[lastname].sandbox.autotraderau.io"
```

**NOTE**: Ingress is immutable. You need to re-create it. So direct ```kubectl apply``` won't do.

So your options are:
* Delete the ingress and re-deploy (via kubectl apply)
* Edit in place (kubectl edit)

```shell
kubectl delete ingress nginx

kubectl apply -f ingress.yaml

kubectl edit ingress nginx
```

**NOTE**: Don't browse immediately. It takes time for DNS to propagate. Let's take a break for 3 minutes.

#### Run a naked pod (not recommended, only for troubleshooting)
This is for a scenario when you want to investigate relating to network.

```shell
kubectl run -i --tty busybox --image=busybox --restart=Never -- sh

nslookup [lastname].sandbox.autotraderau.io

exit
```

Gabriel suggested this for network troubleshooting, it also works well.

```shell
kubectl create deployment multitool --image=wbitt/network-multitool

kubectl get pod

kubectl exec -it [pod-identifier] /bin/bash

nslookup [lastname].sandbox.autotraderau.io

exit
```

Take a look at the pod status:

```shell
kubectl get pod
```

What happened to busybox status?

**DON'T FORGET**:

* Container is mortal. If you run any container, make sure it has something that runs in a loop. If not, the container will terminate.
* Even when you run a naked pod, when it terminates, the pod definition still lingers. Clean it up!

```shell
kubectl delete pod busybox
```

# Deployment with Helm

Helm is deployment tool (similar to RPM or YUM). It's client-server architecture, has a repository to look at. Why we need helm? We want to bundle the entire deployment (deployment, service, ingress) as one. Then we can install, replace, or remove them in one go.

Helm server component runs as container in Kubernetes cluster. It's called Tiller. The client sits on your computer as ```helm``` command.

#### Update helm repository
Fetch the default repo of helm.

```shell
helm repo update
```

#### Search helm repo
Helm repo has many applications (charts), let's find nginx chart.

```shell
helm search nginx
```

#### Add a new repo
Similar to RPM, if you can't find the chart you want, you can add a different repo.

```shell
helm repo add bitnami https://charts.bitnami.com/bitnami

helm repo update

helm search bitnami/nginx
```

#### Install nginx with helm
Let's install nginx and name it as [lastname]-webserver. This name is your identifier to the helm deployment.

```shell
helm install --name [lastname]-webserver bitnami/nginx

kubectl get all
```

There are many things deployed by Helm. Pay attention to the Service. It creates a classic AWS ELB. We already has Zalando ALB and we want to use it. We will adjust this later.

**NOTE**: If you forget to give a name with Helm, like Docker, you will get some fancy name (e.g. everest-koala).

#### List helm deployment
Check what deployment done in helm already.

```shell
helm ls
```

#### Remove the helm deployment
To delete helm deployment, you need to purge it. If you don't helm will think you already a helm deployment with the name you gave. This is problem if you want to deploy with the same name.

```shell
helm delete --purge [lastname]-webserver
```

# Customizing Helm charts
You can download the charts to your local disk.

```shell
helm fetch --untar bitnami/nginx

find nginx -type f
```

Understand the structure:

* README.md: read this, this is your instruction!
* Chart.yaml: the descriptor of the helm chart project
* templates directory: where your deployment, service, ingress lives
* values.yaml: default values for to override anything in template

#### Change default values of helm
We can download the chart, edit it, and save it in our repository.

Let's change the service to ClusterIP:

* Go to ```nginx/templates/svc.yaml``` and pay attention to .spec.type. It expects from .Values.service.type.
* Check ```nginx/README.md``` for variable that can be overridden, can you find service.type? What's its default value?
* Check ```nginx/values.yaml```, now you see why the default value is LoadBalancer
* Edit it to "ClusterIP"

Let's put it via Zalando Ingress and give it DNS name:

* Edit ```nginx/values.yaml``` and give the annotations (see above)
* Comment out or delete tls setting for ingress in ```nginx/values.yaml``` (we don't need tls because we have AWS ACM for certificate in ALB)

Deploy Helm from our local disk:

```shell
helm install --name [lastname]-webserver ./nginx

kubectl get all

kubectl get ingress
```

# Troubleshooting
When you deploy and something is not right, what can you do?

#### Check if the pod is running
You can set the *watch* option to constantly update the state of the pods

```shell
kubectl get po -w
```

#### Get the log from the failed pod
Get the log output from pod and tail it

```shell
kubectl logs [pod-identifier] -f
```

#### Understand what the cluster is doing
May be there are components of the cluster that failed. Check it.

```shell
kubectl get events -w
kubectl get events -n [namespace] -w
```

#### Log into the pod (if it's running)

```shell
kubectl exec -it [pod-identifier] /bin/bash
```
